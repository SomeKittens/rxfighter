import { Observable, Subject, BehaviorSubject } from 'rxjs';

import { config } from './config';
import { GameState } from './gameState';
import { randInt } from './util';

/*
ART:
player spaceship: https://opengameart.org/content/24x24-48x48-spaceships
aliens: https://opengameart.org/content/alien-spaceship-invasion
explosion: https://opengameart.org/content/simple-explosion-bleeds-game-art
*/

let canvas = <HTMLCanvasElement>document.querySelector('canvas');
export let ctx = canvas.getContext('2d');
canvas.width = config.canvas.width;
canvas.height = config.canvas.height;

// Game loop stuff
import { updateStars, renderStars } from './stars';
import { updatePlayer, renderPlayer } from './player';
import { updateEnemies, renderEnemies } from './enemy';
import { renderLasers } from './lasers';
import { checkCollision, renderExplosions } from './collisions';
import { renderGameOver, resetGame } from './gameOver';

let gameLoop$ = new Subject<GameState>();

/* todo:
- different animation sequence for enemy
*/

gameLoop$
.let(resetGame)
.let(updateStars)
.let(updatePlayer)
.let(updateEnemies)
.let(checkCollision)
// Use .do for side effects, ensure render does not mutate state
.do(renderStars)
.do(renderPlayer)
.do(renderLasers)
.do(renderEnemies)
.do(renderExplosions)
.do(renderGameOver)
  .subscribe((state: GameState) => requestAnimationFrame(() => renderFrame(state)));

// Core game loop
function renderFrame(state: GameState) {
  ctx.fillStyle = '#000';
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  gameLoop$.next(state);
}

renderFrame(new GameState());
